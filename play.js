const person = {
    name: 'khanif',
    age: 29,
    greet() {
        console.log('Hi i am ' + this.name);
    }
}
// console.log(person);
// person.greet();

const hobbies = ['Sports', 'Cooking'];
// for (let hobby of hobbies){
//     console.log(hobby);
// }

console.log(hobbies);
console.log(hobbies.map(hobby => 'hobby: ' + hobby));

